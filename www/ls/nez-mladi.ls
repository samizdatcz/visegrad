return unless ig.containers.['nez-mladi']
container = d3.select ig.containers.['nez-mladi']
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Těsně před vstupem do EU byla téměř polovina mladých Poláků bez práce"
yScale = d3.scale.linear!
  ..domain [43.5 0]
xScale = d3.scale.linear!
  ..domain [1990 2015]
data = d3.csv.parse ig.data['nez-mladi'], (row) ->
  row.points = for year in [1990 to 2015]
    value = parseFloat row[year]
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    labelY = row.state + ": #{ig.utils.formatNumber value, 1} %"
    point = {year, value, x, y, labelX, labelY}
    if year == 2002
      row.referencePoint = point
    point
  row
config =
  width: 560
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 135}
  data: data
chartContainer = content.append \div
  ..attr \class \chart-container

defaultPoint = data.2.points.11
chart = new ig.LineChart chartContainer, config
  ..lines.points.attr \r 2
  ..highlight defaultPoint
  ..downlight = -> @highlight defaultPoint
  ..drawing.append \text
    ..attr \class \highlight-2009
    ..text "43,1 %"
    ..attr \transform "translate(#{chart.scaleX defaultPoint.x},#{chart.scaleY defaultPoint.y})"
    ..attr \text-anchor \middle
    ..attr \dy -11

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Nezaměstnanost mladých<br>% z pracovní síly ve věku 15 – 24 let, zdroj: <a href="http://data.worldbank.org/indicator/SL.UEM.1524.ZS">Světová banka</a>'
  ..append \p
    ..html "Najetím myší zobrazíte vývoj všech států V4"

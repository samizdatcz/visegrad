return unless ig.containers['madarsko-mapa']
container = d3.select ig.containers['madarsko-mapa']
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Rozpadem uherské říše přišli Maďaři<br>o 72 % ovládaného území "
mapContainer = content.append \div
  ..attr \class \map-container
map = mapContainer.append \div
  ..attr \class \map

map = L.map do
  * map.node!
  * maxZoom: 7
    minZoom: 5
    zoom: 5
    center: [47.0, 20.4]

baseLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png"
  * zIndex: 1
    opacity: 1
    attribution: 'mapová data &copy; přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'

labelLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png"
  * zIndex: 3
    opacity: 0.75

baseLayer.addTo map
labelLayer.addTo map

dataLayer = L.geoJson ig.data['madarsko-mapa'], {color: \#ab0000}

dataLayer.addTo map

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Uherská říše v roce 1914, zdroj: <a href="https://en.wikipedia.org/wiki/Kingdom_of_Hungary#/media/File:Lands_of_the_Crown_of_Saint_Stephen_in_1914.png">Wikipedie</a>'

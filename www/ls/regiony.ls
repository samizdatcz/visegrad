return unless ig.containers.regiony
container = d3.select ig.containers.regiony
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "České regiony jsou ekonomicky poměrně vyrovnané,<br>Slovensko je na tom hůř "

data = d3.csv.parse ig.data.regiony, (row) ->
  row.ratio = parseFloat row.ratio.replace ',' '.'
  row.pps = parseInt row.pps, 10
  row

countries =
  * abbr: \cz
    name: "Česko"
  * abbr: \hu
    name: "Maďarsko"
  * abbr: \pl
    name: "Polsko"
  * abbr: \sk
    name: "Slovensko"
for country in countries
  country.regions = data.filter (-> it.country is country.abbr)
    ..sort (a, b) -> b.ratio - a.ratio


yScale = d3.scale.linear!
  ..domain [0.41 1]
  ..range [100 0]


content.append \div
  ..attr \class \drawing
  ..append \div
    ..attr \class \scroller
    ..selectAll \div.country .data countries .enter!append \div
      ..attr \class \country
      ..append \div
        ..attr \class \name
        ..html (.name)
      ..append \ul
        ..selectAll \li .data (.regions) .enter!append \li
          ..style \top -> "#{yScale it.ratio}%"
          ..append \div
            ..attr \class \circle
          ..append \span
            ..attr \class \title
            ..html -> "#{ig.utils.formatNumber it.ratio * 100} % – #{it.region}"


content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Disponibilní příjem domácností podle NUTS 2<br>% nejbohatšího regionu, zdroj: <a href="http://ec.europa.eu/eurostat/tgm/table.do?tab=table&plugin=1&language=en&pcode=tgs00026">Eurostat</a>'
  ..append \p
    ..html "Najetím myší zobrazíte ostatní státy V4"

return unless ig.containers.vzdelani
container = d3.select ig.containers.vzdelani
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Česko patří v Evropě k zemím s nejnižšími výdaji na vzdělání"
data = d3.csv.parse ig.data.vzdelani, (row) ->
  row.gdp = parseFloat row.gdp.replace ',' '.'
  row

yScale = d3.scale.linear!
  ..domain [4 5]
  ..range [100 0]
content.append \div
  ..attr \class \drawing
  ..append \div
    ..attr \class \slider
    ..selectAll \div.country .data data .enter!append \div
      ..attr \class \country
      ..style \top -> "#{yScale it.gdp}%"
      ..append \div
        ..attr \class \circle
      ..append \span
        ..attr \class \title
        ..html -> "#{ig.utils.formatNumber it.gdp, 2} % – #{it.country}"

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Veřejné výdaje na vzdělávání (% HDP), zdroj: <a href="http://ec.europa.eu/eurostat/tgm/table.do?tab=table&init=1&language=en&pcode=tsdsc510&plugin=1">Eurostat</a>'
  ..append \p
    ..html "Najetím myší zobrazíte časový vývoj ostatních zemí"

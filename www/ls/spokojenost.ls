return unless ig.containers.spokojenost
container = d3.select ig.containers.spokojenost
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Maďaři patří k nejméně spokojeným národům v Evropě"
data = d3.csv.parse ig.data.spokojenost, (row) ->
  row.satisfaction = parseFloat row.satisfaction
  row

yScale = d3.scale.linear!
  ..domain [10.5 29.3]
  ..range [100 0]
content.append \div
  ..attr \class \drawing
  ..append \div
    ..attr \class \slider
    ..selectAll \div.country .data data .enter!append \div
      ..attr \class \country
      ..style \top -> "#{yScale it.satisfaction}%"
      ..append \div
        ..attr \class \circle
      ..append \span
        ..attr \class \title
        ..html -> "#{ig.utils.formatNumber it.satisfaction, 1} – #{it.country}"
content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Celková spokojenost se životem<br>% populace s odpovědí „vysoká“, zdroj: <a href="http://ec.europa.eu/eurostat/en/web/products-datasets/-/ILC_PW05">Eurostat</a>'
  ..append \p
    ..html "Najetím myší zobrazíte vývoj do roku 2015"

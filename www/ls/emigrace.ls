return unless ig.containers.emigrace
container = d3.select ig.containers.emigrace
months = [2003 to 2013]
data = d3.csv.parse ig.data['emigrace'], (row) ->
  subtotal = 0
  total = 0
  row.months = for month in months
    value = (parseFloat row[month]) || 0
    total += value
    value
  globalSubtotal = total
  row.months = for value in row.months
    globalSubtotal -= value
    {value, subtotal: globalSubtotal}
  row.africa = row.afrika == "TRUE"
  row.total = total
  row.countryName = row.destination
  row
data.sort (a, b) -> b.total - a.total


lineHeight = 36px

xScale = d3.scale.linear!
  ..domain [0 data.0.total]
  ..range [0 80]

content = container.append \div
  ..attr \class \content

content
  ..append \h3
    ..html "Oproti ostatním zemím V4 Poláci<br>často hledají práci jinde"


list = content.append \ul

for datum in data
  datum.element = list.append \li
    ..classed \africa datum.africa
    ..datum datum
    ..append \span
      ..attr \class \title
      ..html (.countryName)
    ..append \div
      ..attr \class \bar
      ..append \div
        ..attr \class \item
        ..style \width -> "#{xScale it.total}%"
      ..append \div
        ..attr \class "count service"
        ..style \right -> "#{xScale it.total}%"
        ..html -> "#{ig.utils.formatNumber it.total}"
      ..append \div
        ..attr \class \months
    ..append \div
      ..attr \class \overlay
monthsLength = months.length
highlightItem = (datum) ->
  if datum.downlighting
    datum.highlightQueued = yes
    return
  datum.highlightQueued = no
  datum.highlighting = yes
  datum.element.classed \active yes
  subItems = datum.element.select \.months
    ..classed \active yes
    ..selectAll \div.month .data datum.months
      ..enter!append \div
        ..style \width -> "#{xScale it.value}%"
      ..style \right -> "#{xScale it.subtotal}%"
      ..attr \class \month
      ..transition!
        ..delay (d, i) -> 200 + i * 25
        ..attr \class "month rotated"
        ..style \right (d, i) -> "#{(monthsLength - i) * 11}px"
    ..append \div
      ..attr \class \year-container
      ..selectAll \div.year .data [2003, 2013] .enter!append \div
        ..attr \class \year
        ..html -> it
        ..transition!
          ..delay (d, i) -> i * 12 * 25
          ..style \opacity 1
  setTimeout do
    ->
      datum.highlighting = no
      if datum.downlightQueued
        downlightItem datum
    200 + datum.months.length * 25

downlightItem = (datum) ->
  if datum.highlighting
    datum.downlightQueued = yes
    return
  datum.downlightQueued = no
  datum.downlighting = yes
  datum.element.classed \active no
  months = datum.element.selectAll \div.month
  months.attr \class "month"
  years = datum.element.selectAll \div.year
    ..style \opacity 0
  <~ setTimeout _, 200
  months.style \right -> "#{xScale it.subtotal}%"
  <~ setTimeout _, 200
  months.attr \class "month tall"
  <~ setTimeout _, 200
  datum.element.select \.months .classed \active no
  months.attr \class "month tall exiting"
  <~ setTimeout _, 200
  datum.element.classed \active no
  years.remove!
  months.remove!
  datum.downlighting = no
  if datum.highlightQueued
    highlightItem datum
container.on \mouseover ->
  for datum in data
    highlightItem datum
container.on \mouseout ->
  for datum in data
    downlightItem datum


content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Příliv cizích státních příslušníků, zdroj: <a href="http://dx.doi.org/10.1787/888933260290">OECD</a>'
  ..append \p
    ..html "Najetím myší zobrazíte časový vývoj migrace do&nbsp;jednotlivých zemí"

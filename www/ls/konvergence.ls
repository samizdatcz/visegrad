return unless ig.containers.['konvergence']
container = d3.select ig.containers.['konvergence']
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Slovensko se kupní síle Evropské unie přibližovalo nejrychleji ze států V4"
yScale = d3.scale.linear!
  ..domain [100 40]
xScale = d3.scale.linear!
  ..domain [1995 2014]
data = d3.csv.parse ig.data['konvergence'], (row) ->
  row.points = for year in [1995 to 2014]
    value = parseFloat do
      row[year].replace '%' '' .replace ',' '.'
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    labelY = row.state + ": #{ig.utils.formatNumber value, 1} %"
    point = {year, value, x, y, labelX, labelY}
    point
  row

config =
  width: 560
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 150}
  data: data
chartContainer = content.append \div
  ..attr \class \chart-container


chart = new ig.LineChart chartContainer, config
  ..lines.points.attr \r 2
legend = content.append \ul
  ..attr \class \legend
  ..selectAll \li .data data .enter!append \li
    ..html -> it.state

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'HDP na hlavu v PPP (% EU28), zdroj: <a href="http://stats.oecd.org/Index.aspx?DataSetCode=PDB_LV">OECD</a>'
  ..append \p
    ..html "Najetím myší zobrazíte celý časový vývoj vybraných zemí"

return unless ig.containers['export']
container = d3.select ig.containers['export']
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Slováci exportují do Evropy nejvíc z V4"
yScale = d3.scale.linear!
  ..domain [88 56]
xScale = d3.scale.linear!
  ..domain [2003 2014]
data = d3.csv.parse ig.data.export, (row) ->
  row.points = for year in [2003 to 2014]
    value = parseFloat row[year].replace ',' '.'
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    labelY = row.state + " – " + ig.utils.formatNumber value, 1
    point = {year, value, x, y, labelX, labelY}
    if year == 2014
      row.referencePoint = point
    point
  row
data.length = 4
config =
  width: 350
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 125}
  data: data
chartContainer = content.append \div
  ..attr \class \chart-container
chart = new ig.LineChart chartContainer, config
chartContainer.append \div
  ..attr \class \default
  ..append \div
    ..attr \class \x-label
      ..append \div
        ..attr \class \extent
      ..append \div
        ..attr \class \label
        ..html 2014
  ..append \div
    ..selectAll \div .data data .enter!append \div
      ..style \top -> "#{config.padding.top + chart.height * it.referencePoint.y}px"
      ..attr \class \item
      ..append \label
        ..html -> "#{it.state} – #{ig.utils.formatNumber it.referencePoint.value, 1} % HDP"
      ..append \div
        ..attr \class \circle
chartContainer.append \div
  ..attr \class \cover

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Export do EU (% celkového exportu), zdroj: <a href="http://ec.europa.eu/eurostat/tgm/table.do?tab=table&init=1&language=en&pcode=tet00036&plugin=1">Eurostat</a>'
  ..append \p
    ..html "Najetím myší zobrazíte celý časový vývoj od roku 2003"

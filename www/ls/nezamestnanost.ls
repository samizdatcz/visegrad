return unless ig.containers['nezamestnanost']
container = d3.select ig.containers['nezamestnanost']
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Na Slovensku je nejvyšší nezaměstnanost, dvě třetiny jsou nezaměstnaní dlouhodobě"
yScale = d3.scale.linear!
  ..domain [20 0]
xScale = d3.scale.linear!
  ..domain [1991 2014]
data = d3.csv.parse ig.data.nezamestnanost, (row) ->
  row.points = for year in [1991 to 2014]
    value = parseFloat row[year]
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    labelY = row.state + " – " + ig.utils.formatNumber value, 1
    point = {year, value, x, y, labelX, labelY}
    if year == 2014
      row.referencePoint = point
    point
  row

config =
  width: 550
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 125}
  data: data

chartContainer = content.append \div
  ..attr \class \chart-container
chart = new ig.LineChart chartContainer, config
chartContainer.append \div
  ..attr \class \default
  ..append \div
    ..attr \class \x-label
      ..append \div
        ..attr \class \extent
      ..append \div
        ..attr \class \label
        ..html 2014
  ..append \div
    ..selectAll \div .data data .enter!append \div
      ..style \top -> "#{config.padding.top + chart.height * it.referencePoint.y}px"
      ..attr \class \item
      ..append \label
        ..html -> "#{it.state} – #{ig.utils.formatNumber it.referencePoint.value, 1} %"
      ..append \div
        ..attr \class \circle
chartContainer.append \div
  ..attr \class \cover

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Nezaměstnanost (% z pracovní síly), zdroj: <a href="http://data.worldbank.org/indicator/SL.UEM.LTRM.ZS/">Světová banka</a>'
  ..append \p
    ..html "Najetím myší zobrazíte vývoj od roku 1991"

return unless ig.containers.chudoba
container = d3.select ig.containers.chudoba
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Po vstupu do EU se chudoba v Polsku snížila, přesto je z V4 nejhorší "
yScale = d3.scale.linear!
  ..domain [21 0]
xScale = d3.scale.linear!
  ..domain [2000 2015]
data = d3.csv.parse ig.data.chudoba, (row) ->
  row.points = for year in [2000 to 2013]
    value = parseFloat row[year].replace ',' '.'
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    labelY = row.state + " – " + "#{ig.utils.formatNumber value, 1} %"
    point = {year, value, x, y, labelX, labelY}
    if year == 2012
      row.referencePoint = point
    point
  row
config =
  width: 350
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 125}
  data: data
chartContainer = content.append \div
  ..attr \class \chart-container
chart = new ig.LineChart chartContainer, config
chartContainer.append \div
  ..attr \class \default
  ..append \div
    ..attr \class \x-label
      ..append \div
        ..attr \class \extent
      ..append \div
        ..attr \class \label
        ..html 2012
  ..append \div
    ..selectAll \div .data data .enter!append \div
      ..style \top -> "#{config.padding.top + chart.height * it.referencePoint.y}px"
      ..attr \class \item
      ..append \label
        ..html -> "#{it.state} – #{ig.utils.formatNumber it.referencePoint.value} %"
      ..append \div
        ..attr \class \circle
chartContainer.append \div
  ..attr \class \cover

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Podíl obyvatel pod hladinou chudoby (% populace)<br>zdroj: <a href="http://data.worldbank.org/indicator/SI.POV.NAHC">Světová banka</a>'
  ..append \p
    ..html "Najetím myší zobrazíte vývoj od roku 2000"

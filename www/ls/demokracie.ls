return unless ig.containers.demokracie
container = d3.select ig.containers.demokracie
data = d3.csv.parse ig.data.demokracie, (row) ->
  for field, value of row
    continue if field == "Země"
    row[field] = parseInt value, 10
  row
fieldsDefault =
  "Velmi úspěšné"
  "Spíš úspěšné"
fieldsExtended =
  "Netuším"
  "Spíš neúspěšné"
  "Velmi neúspěšné"

createLabel = (d, i, ii) ->
  v = data[ii][d]
  o = v.toString!
  if v >= 15 then o += " %"
  o

container
  ..append \div
    ..attr \class \content
    ..append \h3
      ..html "Maďarům se demokracie nezamlouvá"
    ..append \div
      ..attr \class \foot
      ..html "Odpovědi na otázku<br><em>Budování demokracie v naší zemi v&nbsp;minulých dvaceti letech bylo:</em>"
      ..append \ul
        ..attr \class \legend
        ..selectAll \li.default .data fieldsDefault .enter!append \li
          ..attr \class \default
          ..html -> it
        ..selectAll \li.extended .data fieldsExtended .enter!append \li
          ..attr \class \extended
          ..html -> it
    ..append \div
      ..attr \class \bars
      ..selectAll \div.row .data data .enter!append \div
        ..attr \class \row
        ..append \span
          ..attr \class \row-name
          ..html -> it['Země']
        ..append \div
          ..attr \class \fields
          ..append \div
            ..attr \class \default
            ..selectAll \div.field .data fieldsDefault .enter!append \div
              ..attr \class \field
              ..style \width (d, i, ii) -> "#{data[ii][d]}%"
              ..append \span
                ..attr \class \label
                ..html createLabel
            ..append \div
              ..attr \class \sum-label
              ..html (datum) ->
                fieldsDefault
                  .reduce do
                    (prev, curr) ->
                      prev + datum[curr]
                    0
                  .toString! + " %"
          ..append \div
            ..attr \class \extended
            ..selectAll \div.field .data fieldsExtended.reverse! .enter!append \div
              ..attr \class \field
              ..style \width (d, i, ii) -> "#{data[ii][d]}%"
              ..append \span
                ..attr \class \label
                ..html createLabel
    ..append \div
      ..attr \class \footer
      ..append \p
        ..html 'Průzkum veřejného mínění, zdroj: <a href="http://www.ivo.sk/5855/sk/aktuality/ivo-organizoval-tlacovu-besedu-%E2%80%9Enavrat-do-europy%E2%80%9C">Inštitút pre verejné otázky</a> '
      ..append \p
        ..html "Najetím myší zobrazíte zbývající odpovědi"

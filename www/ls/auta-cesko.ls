return unless ig.containers.['auta-cesko']
container = d3.select ig.containers.['auta-cesko']
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Od vstupu do EU vyrobí Slováci a Češi celosvětově nejvíc aut na obyvatele"
yScale = d3.scale.linear!
  ..domain [180 0]
xScale = d3.scale.linear!
  ..domain [1997 2014]
data = d3.csv.parse ig.data['auta'], (row) ->
  row.points = for year in [1997 to 2014]
    value = parseFloat do
      row[year].replace ',' '.'
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    labelY = row.state + ": #{ig.utils.formatNumber value, 1}"
    point = {year, value, x, y, labelX, labelY}
    point
  row

config =
  width: 560
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 125}
  data: data
chartContainer = content.append \div
  ..attr \class \chart-container


chart = new ig.LineChart chartContainer, config
  ..lines.points.attr \r 2
legend = content.append \ul
  ..attr \class \legend
  ..selectAll \li .data data .enter!append \li
    ..html -> it.state

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Vyrobená auta (na 1000 obyvatel), zdroj: <a href="http://www.oica.net/category/production-statistics/">OICA</a>'
  ..append \p
    ..html "Najetím myší zobrazíte celý časový vývoj vybraných zemí"

return unless ig.containers.media
container = d3.select ig.containers.media
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "V Maďarsku jsou nejméně<br>svobodná média z celé V4"
yScale = d3.scale.linear!
  ..domain [0 68]
xScale = d3.scale.linear!
  ..domain [2002 2015]
data = d3.csv.parse ig.data.media, (row) ->
  row.points = for year in [2002 to 2015]
    value = parseFloat row[year].replace ',' '.'
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    labelY = row.state + " – " + ig.utils.formatNumber value, 1
    point = {year, value, x, y, labelX, labelY}
    if year == 2015
      row.referencePoint = point
    point
  row
config =
  width: 350
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 125}
  data: data
chartContainer = content.append \div
  ..attr \class \chart-container
chart = new ig.LineChart chartContainer, config
chartContainer.append \div
  ..attr \class \default
  ..append \div
    ..attr \class \x-label
      ..append \div
        ..attr \class \extent
      ..append \div
        ..attr \class \label
        ..html 2015
  ..append \div
    ..selectAll \div .data data .enter!append \div
      ..style \top -> "#{config.padding.top + chart.height * it.referencePoint.y}px"
      ..attr \class \item
      ..append \label
        ..html -> "#{it.state} – #{ig.utils.formatNumber it.referencePoint.value}"
      ..append \div
        ..attr \class \circle
chartContainer.append \div
  ..attr \class \cover
content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Index svobody médií, zdroj: <a href="index.rsf.org">Reporters without Borders</a>'
  ..append \p
      ..html "Najetím myší zobrazíte vývoj od roku 2002"

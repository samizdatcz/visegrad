return unless ig.containers.['hdpzmena-slovensko']
container = d3.select ig.containers.['hdpzmena-slovensko']
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Euro nezachránilo Slovensko před<br>krizovým propadem HDP"
yScale = d3.scale.linear!
  ..domain [12 -13]
xScale = d3.scale.linear!
  ..domain [1990 2015]
data = d3.csv.parse ig.data.hdpzmena, (row) ->
  row.points = for year in [1990 to 2015]
    value = parseFloat row[year]
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    sign = if value > 0 then "+" else ""
    labelY = row.state + ": #sign #{ig.utils.formatNumber value, 1} %"
    point = {year, value, x, y, labelX, labelY}
    point
  row
config =
  width: 560
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 135}
  data: data
chartContainer = content.append \div
  ..attr \class \chart-container

defaultPoint = data.3.points.15
chart = new ig.LineChart chartContainer, config
  ..lines.points.attr \r 2
  ..drawing.append \g
    ..attr \class \zero-line
    ..attr \transform "translate(0, #{chart.scaleY yScale 0})"
    ..append \line
      ..attr \x2 chart.width - 7
    ..append \text
      ..text "0 %"
      ..attr \text-anchor \end
      ..attr \x -8
      ..attr \dy 4
  ..highlight defaultPoint
  ..downlight = -> @highlight defaultPoint
  ..drawing.append \text
    ..attr \class \highlight-2009
    ..text "- 6,3 %"
    ..attr \transform "translate(#{chart.scaleX defaultPoint.x},#{chart.scaleY defaultPoint.y})"
    ..attr \text-anchor \middle
    ..attr \dy 5
    ..attr \dx -30

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Růst reálného HDP (%), zdroj: <a href="http://www.imf.org/external/pubs/ft/weo/2016/update/01/">Mezinárodní měnový fond</a>'
  ..append \p
    ..html "Najetím myší zobrazíte vývoj všech států V4"

return unless ig.containers.['korupce']
container = d3.select ig.containers.['korupce']
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Vnímání korupce se v posledních<br>letech v Česku lepší"
yScale = d3.scale.linear!
  ..domain [80 20]
xScale = d3.scale.linear!
  ..domain [1996 2015]
data = d3.csv.parse ig.data['korupce'], (row) ->
  row.points = for year in [1996 to 2015]
    value = parseFloat do
      row[year].replace '%' '' .replace ',' '.'
    value *= 10
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    labelY = row.state + ": #{ig.utils.formatNumber value}"
    point = {year, value, x, y, labelX, labelY}
    point
  row

config =
  width: 560
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 125}
  data: data
chartContainer = content.append \div
  ..attr \class \chart-container


chart = new ig.LineChart chartContainer, config
  ..lines.points.attr \r 2
legend = content.append \ul
  ..attr \class \legend
  ..selectAll \li .data data .enter!append \li
    ..html -> it.state

content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Index vnímání korupce (1 nejhorší, 100 nejlepší)<br>zdroj: <a href="http://www.transparency.org/research/cpi/">Transparency International</a>'
  ..append \p
    ..html "Najetím myší zobrazíte časový vývoj ostatních zemí"

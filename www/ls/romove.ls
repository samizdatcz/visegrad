return unless ig.containers.romove
container = d3.select ig.containers.romove
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Romové tvoří přibližně desetinu slovenského obyvatelstva"
data = d3.csv.parse ig.data.romove

content.append \div
  ..attr \class \table-container
  ..append \table
    ..append \thead
      ..append \tr
        ..append \th .html ""
        ..append \th .html "Počet"
        ..append \th .html "Podíl"
    ..append \tbody
      ..selectAll \tr .data data .enter!append \tr
        ..append \th .html (.country)
        ..append \td .html (.absolute)
        ..append \td .html (.relative)


content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Romská diaspora, zdroj: <a href="https://en.wikipedia.org/wiki/Romani_diaspora">Wikipedia</a>'
  ..append \p
    ..html "Najetím myší zobrazíte podíl Romů v populaci"

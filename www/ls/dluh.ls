return unless ig.containers.dluh
container = d3.select ig.containers.dluh
content = container.append \div
  ..attr \class \content
  ..append \h3
    ..html "Maďarsko zdědilo vysoký státní<br>dluh, trápí ho dodnes"
yScale = d3.scale.linear!
  ..domain [84 0]
xScale = d3.scale.linear!
  ..domain [1991 2010]
data = d3.csv.parse ig.data.dluh, (row) ->
  row.points = for year in [1991 to 2010]
    value = parseFloat row[year].replace ',' '.'
    continue unless value
    x = xScale year
    y = yScale value
    labelX = year
    labelY = row.state + " – " + ig.utils.formatNumber value, 1
    point = {year, value, x, y, labelX, labelY}
    if year == 1993
      row.referencePoint = point
    point
  row
config =
  width: 350
  height: 250
  padding: {top: 25, right: 25, bottom: 35, left: 125}
  data: data
chartContainer = content.append \div
  ..attr \class \chart-container
chart = new ig.LineChart chartContainer, config
chartContainer.append \div
  ..attr \class \default
  ..append \div
    ..attr \class \x-label
      ..append \div
        ..attr \class \extent
      ..append \div
        ..attr \class \label
        ..html 1993
  ..append \div
    ..selectAll \div .data data .enter!append \div
      ..style \top -> "#{config.padding.top + chart.height * it.referencePoint.y}px"
      ..attr \class \item
      ..append \label
        ..html -> "#{it.state} – #{ig.utils.formatNumber it.referencePoint.value, 1} % HDP"
      ..append \div
        ..attr \class \circle
chartContainer.append \div
  ..attr \class \cover
content.append \div
  ..attr \class \footer
  ..append \p
    ..html 'Vládní dluh (% HDP), zdroj: <a href="http://stats.oecd.org/index.aspx?queryid=8089">OECD</a> '
  ..append \p
    ..html "Najetím myší zobrazíte vývoj do roku 2010"
